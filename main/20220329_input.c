/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sdkconfig.h"
#include "input.h"
#include "output.h"
#define BLINK_GPIO 2
void input_callback(int pin)
{
    if(pin == 4)
    {
        output_toggle(2);
    }
}
void app_main(void)
{
    output_create(2);
    input_create(4, HIGH_TO_LOW);
    input_set_callback(input_callback);
    while(1) {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
